
This application operates with the database of railway roads, adds, deletes and takes routes.

For calculating the shortest route between two cities the application uses [Dijkstra's algorithm](https://ru.wikipedia.org/wiki/%D0%90%D0%BB%D0%B3%D0%BE%D1%80%D0%B8%D1%82%D0%BC_%D0%94%D0%B5%D0%B9%D0%BA%D1%81%D1%82%D1%80%D1%8B). 

The database contains two tables:
- city (id and name);
- route(id of the route, id cities of the  start and the end of the route, time, length)

---
## List of commands

## route [CITY_FROM] [CITY_TO]
calculates the shortest route between two cities and returns result with parameters of the route(all the cities that pass through the route, length)

**Example:**
```
route Paris Madrid
```
**Result:**
```
Route: Paris Marsel Madrid takes 5842 kilometers
```
## help

prints all available commands

**Example:**
```
help
```

**Result:**
```
Possible commands: help, cities, route, add-city, add-route, delete-city, delete-route
```

## cities

prints all cities from the database

**Example:**
```
cities
```
**Result:**
```
Paris
Brussels
Berlin
Rome
Marsel
Madrid
```
## add-city  [CITY_NAME]

adds new record to the database (table city) with new city, checks, if the city is already exist in the database

**Example:**
```
add-city Bern
```
**Result:**
```
city Bern successfully added to database
```

## add-route  [CITY_FROM] [CITY_TO] [ROUTE_TIME] [ROUTE_LENGTH]

adds new record to the database (table route) with parameters of the route. Start point and point of destination are written by id, which taken from table city.

Merely one city from new route must have connection to a city from database

**Example:**
```
add-route Berlin Praga 452 2145
```
**Result:**
```
Route from Berlin to Praga successfully added to database
```
## delete-city  [CITY_NAME] 

deletes city from the database (table city and simultaneously table route)

**Example:**
```
delete-city Bern
```
**Result:**
```
city Bern successfully deleted from database
```
## delete-route  [CITY_FROM] [CITY_TO]

deletes route from the database (table route)

**Example:**
```
delete-route Berlin Praga
```
**Result:**
```
Route from Berlin to Praga successfully deleted from the database
```
## Initialization Database

```
CREATE TABLE city (
    id          SERIAL PRIMARY KEY,
    name 		varchar(30)
);

CREATE TABLE route(
	id  SERIAL PRIMARY KEY,
	route_from integer REFERENCES city (id) ON DELETE CASCADE,
	route_to  integer REFERENCES city (id) ON DELETE CASCADE,
	route_time integer,
	route_length integer	
);

INSERT INTO city (name)
VALUES ('Paris'),
('Brussels'),
('Berlin'),
('Rome'),
('Marsel'),
('Madrid');


INSERT INTO route (route_from, route_to, route_time, route_length)
VALUES ((SELECT id from city WHERE name='Brussels'), (SELECT id from city WHERE name='Berlin'), 162, 635),
((SELECT id from city WHERE name='Paris'), (SELECT id from city WHERE name='Brussels'), 187, 800),
((SELECT id from city WHERE name='Brussels'), (SELECT id from city WHERE name='Rome'), 372, 2132),
((SELECT id from city WHERE name='Marsel'), (SELECT id from city WHERE name='Brussels'), 315, 1528),
((SELECT id from city WHERE name='Paris'), (SELECT id from city WHERE name='Marsel'), 283, 1119),
((SELECT id from city WHERE name='Marsel'), (SELECT id from city WHERE name='Madrid'), 548, 4723),
((SELECT id from city WHERE name='Madrid'), (SELECT id from city WHERE name='Rome'), 632, 6123);


```