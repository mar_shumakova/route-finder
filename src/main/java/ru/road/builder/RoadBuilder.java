package ru.road.builder;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RoadBuilder implements AutoCloseable {

    private static final String DB_URL = "jdbc:postgresql://127.0.0.1:5432/postgres";
    private static final String USER = "postgres";
    private static final String PASS = "Postgres";
    private Connection conn = null;

    public RoadBuilder() throws RoadBuilderException {
        try {
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
        } catch (SQLException e) {
            throw new RoadBuilderException("Error during connection to database", e);
        }
    }

    /**
     * Finds all distinct cities from database
     *
     * @return list of objects of City type
     * @throws {@link RoadBuilderException} if an error occurred during connection to database
     */
    public List<City> getCities() throws RoadBuilderException {
        List<City> listOfCities = new ArrayList<>();
        ResultSet resultSet = null;
        try {
            try (PreparedStatement readStatement = conn.prepareStatement("SELECT * FROM city;")) {
                resultSet = readStatement.executeQuery();
                while (resultSet.next()) {
                    listOfCities.add(new City(resultSet.getInt(1), resultSet.getString(2)));
                }
            }
        } catch (SQLException e) {
            throw new RoadBuilderException("Error during connection to database", e);
        }
        return listOfCities;
    }


    /**
     * Calculates the shortest route between 2 cities, using Dijkstra's algorithm
     *
     * @param from the name of the city - the starting point of the route
     * @param to   destination city name
     * @return string value containing parameters of the route
     * @throws RoadBuilderException if an error occurred during connection to database
     */
    public String calculateRoute(String from, String to) throws RoadBuilderException {
        StringBuilder sbRoute = new StringBuilder();

        ResultSet resultSet = null;
        int countOfCities = 0;

        //defining the count of cities the size of lists, matrix, etc.)
        try {
            try (PreparedStatement readStatement = conn.prepareStatement("SELECT COUNT(id) FROM city;")) {
                resultSet = readStatement.executeQuery();
                if (resultSet.next()) {
                    countOfCities = resultSet.getInt(1);
                }
            }
        } catch (SQLException e) {
            throw new RoadBuilderException("Error during connection to database", e);
        }

        int[][] routes = new int[countOfCities][countOfCities];
        int[] minRoute = new int[countOfCities];
        int[] markedCities = new int[countOfCities];
        int cityIndex, minLength;
        int defaultValue = 10000;


        // checking the existing of cities in the database
        if (getCityByName(from) == null || getCityByName(to) == null) {
            throw new RoadBuilderException("There is no such city in the database");
        }

        Map<String, City> namesCities = new HashMap<>();
        List<Route> listOfRoutes = new ArrayList<>();
        List<City> listOfCities = getCities();

        // receiving list of routes
        try {
            try (PreparedStatement readStatement = conn.prepareStatement("SELECT route.id, c1.name, " +
                    "c2.name, route_time, route_length FROM route " +
                    "join city as c1 on c1.id = route.route_from " +
                    "join city as c2 on c2.id = route.route_to;")) {
                resultSet = readStatement.executeQuery();
                while (resultSet.next()) {
                    listOfRoutes.add(new Route(resultSet.getInt(1), resultSet.getString(2),
                            resultSet.getString(3), resultSet.getInt(4),
                            resultSet.getInt(5)));
                }
            }
        } catch (SQLException e) {
            throw new RoadBuilderException("Error during connection to database", e);
        }

        // assigning an index to each city (a place in a 2d-array)
        int ind = 1;
        for (City city : listOfCities) {
            if (city.getName().equals(from)) {
                city.setIndex(0);
            } else if (city.getName().equals(to)) {
                city.setIndex(countOfCities - 1);
            } else {
                city.setIndex(ind);
                ind++;
            }
            namesCities.put(city.getName(), city);
        }

        // filling the multidimensional array by route lengths for the further calculating
        for (Route route : listOfRoutes) {
            City cityFrom = namesCities.get(route.getRouteFrom());
            City cityTo = namesCities.get(route.getRouteTo());
            routes[cityFrom.getIndex()][cityTo.getIndex()] = route.getRouteLength();
            routes[cityTo.getIndex()][cityFrom.getIndex()] = route.getRouteLength();
        }

        //searching for max default value
        try {
            try (PreparedStatement readStatement = conn.prepareStatement("SELECT MAX(route_length) FROM route;")) {
                resultSet = readStatement.executeQuery();
                if (resultSet.next()) {
                    if (defaultValue < resultSet.getInt(1)) {
                        defaultValue = resultSet.getInt(1) + 1;
                    }
                }
            }

            // initializing all points(cities) and routes by default values
            for (int i = 0; i < routes.length; i++) {
                minRoute[i] = defaultValue;
                markedCities[i] = 1;
            }

            // defining the start point
            minRoute[0] = 0;

            // defining the shortest way (minRoute[]) from the start point(routes[0][0]) to each city
            do {
                cityIndex = defaultValue;
                minLength = defaultValue;

                // defining the current point to start
                for (int i = 0; i < minRoute.length; i++) {
                    if ((markedCities[i] == 1) && (minRoute[i] < minLength)) {
                        minLength = minRoute[i];
                        cityIndex = i;
                    }
                }

                // iterating the array of a current city
                // checking the min length from the current city to each city
                // if the calculated length is smaller than min length, changing the min length
                if (cityIndex != defaultValue) {
                    for (int i = 0; i < minRoute.length; i++) {
                        if (routes[cityIndex][i] > 0) {
                            int temp = minLength + routes[cityIndex][i];
                            if (temp < minRoute[i]) {
                                minRoute[i] = temp;
                            }
                        }
                    }

                    // marking city point as "viewed"
                    markedCities[cityIndex] = 0;
                }
            } while (cityIndex < defaultValue);

            // rebuilding the route between required cities by the shortest path
            int endPoint = countOfCities - 1;
            int beginPoint = 0;
            int[] pointsOfRoute = new int[markedCities.length];

            // defining the start point - the city of destination
            pointsOfRoute[0] = endPoint + 1;
            int prevPoint = 1;

            // defining the length from routes[0][0] to city "to"
            int routeLength = minRoute[endPoint];

            while (endPoint != beginPoint) {
                for (int i = 0; i < pointsOfRoute.length; i++) {
                    if (routes[i][endPoint] != 0) {
                        int temp = routeLength - routes[i][endPoint];
                        if (temp == minRoute[i]) {
                            routeLength = temp;
                            endPoint = i;
                            pointsOfRoute[prevPoint] = i + 1;
                            prevPoint++;
                        }
                    }
                }
            }

            // building the route (String message) and the shortest length through all the points(cities)
            sbRoute.append("Route: ");
            for (int i = pointsOfRoute.length - 1; i >= 0; i--) {
                if (pointsOfRoute[i] != 0) {
                    final int temp = i;
                    City city = listOfCities.stream().filter(
                            f -> f.getIndex() + 1 == pointsOfRoute[temp]).findFirst().orElse(null);
                    if (city != null) {
                        sbRoute.append(city.getName()).append(" ");
                    }
                }
            }
            sbRoute.append("takes ").append(minRoute[minRoute.length - 1] - minRoute[beginPoint])
                    .append(" kilometers");
        } catch (SQLException e) {
            throw new RoadBuilderException("Error during connection to database", e);
        }
        return sbRoute.toString();
    }

    /**
     * Adds new city to the database
     *
     * @param name contains the name of the city
     * @throws {@link RoadBuilderException} if an error occurred during connection to database
     *                or this city is already exist in the database
     */
    public void addCity(String name) throws RoadBuilderException {
        try {
            try (PreparedStatement insertStatement = conn
                    .prepareStatement("INSERT INTO city (name) VALUES (?);")) {
                if (getCityByName(name) != null) {
                    throw new RoadBuilderException("This city is already exist in the database");
                } else {
                    insertStatement.setString(1, name);
                    insertStatement.executeUpdate();
                }
            }
        } catch (SQLException e) {
            throw new RoadBuilderException("Error during connection to database", e);
        }

    }

    /**
     * Adds new route to the database
     *
     * @param from   the name of the city - the starting point of the route
     * @param to     destination city name
     * @param time   the time spending for the route
     * @param length the length of the route
     * @throws RoadBuilderException if an error occurred during connection to database
     *                     or this route is already exist in the database
     *                     or the route has no connection to other routes
     */
    public void addRoute(String from, String to, int time, int length) throws RoadBuilderException {
        try {
            if (getRoute(from, to) != null) {
                throw new RoadBuilderException("This route is already exist in the database");
            }
            if (getCityByName(from) == null) {
                if (getCityByName(to) == null) {
                    throw new RoadBuilderException("Error: cities have no connection to cities from the database");
                } else {
                    addCity(from);
                }
            } else if (getCityByName(to) == null) {
                addCity(to);
            }
            int indexFrom = getCityByName(from).getId();
            int indexTo = getCityByName(to).getId();
            try (PreparedStatement insertStatement = conn
                    .prepareStatement("INSERT INTO route (route_from, route_to, route_time," +
                            " route_length) VALUES (?, ?, ?, ?);")) {
                insertStatement.setInt(1, indexFrom);
                insertStatement.setInt(2, indexTo);
                insertStatement.setInt(3, time);
                insertStatement.setInt(4, length);
                insertStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new RoadBuilderException("Error during connection to database", e);
        }
    }

    /**
     * Deletes city from the database
     *
     * @param name contains the name of the city
     * @throws RoadBuilderException if an error occurred during connection to the database
     */
    public void deleteCity(String name) throws RoadBuilderException {
        try {
            try (PreparedStatement deleteStatement = conn.prepareStatement("DELETE from city WHERE name=?")) {
                deleteStatement.setString(1, name);
                deleteStatement.executeUpdate();
            }
        } catch (SQLException e) {
            throw new RoadBuilderException("Error during connection to database", e);
        }
    }

    /**
     * Deletes route from the database
     *
     * @param from the name of the city - the starting point of the route
     * @param to   destination city name
     * @throws RoadBuilderException if an error occurred during connection to the database
     */
    public void deleteRoute(String from, String to) throws RoadBuilderException {
        int indexFrom = 0;
        int indexTo = 0;
        if (getRoute(from, to) == null) {
            throw new RoadBuilderException("Error: there is no such route in the database");
        } else {
            indexFrom = getCityByName(from).getId();
            indexTo = getCityByName(to).getId();
        }
        try {
            try (PreparedStatement deleteStatement = conn.prepareStatement("DELETE FROM route WHERE route_from = ?" +
                    " AND route_to = ?;")) {
                deleteStatement.setInt(1, indexFrom);
                deleteStatement.setInt(2, indexTo);
                deleteStatement.executeUpdate();
            }
        }catch (SQLException e) {
            throw new RoadBuilderException("Error during connection to database", e);
        }
    }

    /**
     * Takes the city from database by name
     *
     * @param name of the city
     * @return new Object of City type or null if this city is not exist in the database
     * @throws RoadBuilderException if an error occurred during connection to the database
     */
    private City getCityByName(String name) throws RoadBuilderException {
        ResultSet resultSet = null;
        try {
            try (PreparedStatement readStatement = conn.prepareStatement("SELECT * FROM city WHERE name=?;")) {
                readStatement.setString(1, name);
                resultSet = readStatement.executeQuery();
                if (resultSet.next()) {
                    return new City(resultSet.getInt(1), resultSet.getString(2));
                } else {
                    return null;
                }
            }
        } catch (SQLException e) {
            throw new RoadBuilderException("Error during connection to database", e);
        }
    }

    /**
     * Takes the route from database (it's only data record, not the shortest route)
     *
     * @param from the name of the city - the starting point of the route
     * @param to   destination city name
     * @return new object of Route type
     * @throws RoadBuilderException if an error occurred during connection to the database
     */
    private Route getRoute(String from, String to) throws RoadBuilderException {
        ResultSet resultSet = null;
        try {
            try (PreparedStatement readStatement = conn.prepareStatement(
                    "SELECT route.id, c1.name, c2.name, route_time, route_length FROM route  " +
                            "join city as c1 on c1.id = route.route_from " +
                            "join city as c2 on c2.id = route.route_to " +
                            "where c1.name = ? AND c2.name = ?;")) {
                readStatement.setString(1, from);
                readStatement.setString(2, to);
                resultSet = readStatement.executeQuery();
                if (resultSet.next()) {
                    return new Route(resultSet.getInt(1), resultSet.getString(2),
                            resultSet.getString(3), resultSet.getInt(4),
                            resultSet.getInt(5));
                } else {
                    return null;
                }
            }
        }catch (SQLException e) {
            throw new RoadBuilderException("Error during connection to database", e);
        }
    }

    @Override
    public void close() throws Exception {
        if (conn != null)
            conn.close();
    }
}
