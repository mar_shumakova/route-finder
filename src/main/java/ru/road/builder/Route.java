package ru.road.builder;

public class Route {
    private final int id;
    private final String routeFrom;
    private final String routeTo;
    private final int routeTime;
    private final int routeLength;

    public Route(int id, String routeFrom, String routeTo, int routeTime, int routeLength) {
        this.id = id;
        this.routeFrom = routeFrom;
        this.routeTo = routeTo;
        this.routeTime = routeTime;
        this.routeLength = routeLength;
    }

    public String getRouteFrom() {
        return routeFrom;
    }

    public String getRouteTo() {
        return routeTo;
    }

    public int getRouteLength() {
        return routeLength;
    }
}
