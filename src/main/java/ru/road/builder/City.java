package ru.road.builder;

public class City {
    private final int id;
    private final String name;
    private int index;

    public City(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setIndex(int i){
        this.index = i;
    }

    public int getIndex(){
        return index;
    }
}
