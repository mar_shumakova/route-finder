package ru.road.builder;

import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        try (RoadBuilder roadBuilder = new RoadBuilder()) {
            try (Scanner console = new Scanner(System.in)) {
                String operation = console.nextLine();
                String[] operationPlusParam = operation.split(" ");
                switch (operationPlusParam[0]) {
                    case "help":
                        System.out.println("Possible commands: help, cities, route," +
                                " add-city, add-route, delete-city, delete-route");
                        break;
                    case "cities":
                        List<City> cities = roadBuilder.getCities();
                        cities.stream()
                                .map(City::getName)
                                .forEach(System.out::println);
                        break;
                    case "route":
                        System.out.println(roadBuilder.calculateRoute(operationPlusParam[1], operationPlusParam[2]));
                        break;
                    case "add-city":
                        roadBuilder.addCity(operationPlusParam[1]);
                        System.out.println("city " + operationPlusParam[1] + " successfully added to the database");
                        break;
                    case "add-route":
                        roadBuilder.addRoute(operationPlusParam[1], operationPlusParam[2],
                                Integer.parseInt(operationPlusParam[3]), Integer.parseInt(operationPlusParam[4]));
                        System.out.println("Route from " + operationPlusParam[1] + " to " + operationPlusParam[2] +
                                " successfully added to the database");
                        break;
                    case "delete-city":
                        roadBuilder.deleteCity(operationPlusParam[1]);
                        System.out.println("city " + operationPlusParam[1] + " successfully deleted from the database");
                        break;
                    case "delete-route":
                        roadBuilder.deleteRoute(operationPlusParam[1], operationPlusParam[2]);
                        System.out.println("Route from " + operationPlusParam[1] + " to " + operationPlusParam[2]
                                + " successfully deleted from the database");
                        break;
                    default:
                        System.out.println("Unpredictable command");
                }
            }

        } catch (RoadBuilderException e) {
            System.out.println(e.getMessage());
        } catch (Exception e) {
            System.out.println("Unknown Error: " + e.getMessage());
        }
    }
}
