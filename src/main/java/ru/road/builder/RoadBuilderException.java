package ru.road.builder;

public class RoadBuilderException extends Exception {
    public RoadBuilderException(String message) {
        super(message);
    }

    public RoadBuilderException(String message, Throwable cause) {
        super(message, cause);
    }
}
